####
## Exemplos R
#

#### Setup ####
source("_Setup.R")

#### Processamento ####

# Converte para tibble 
iris %>% 
  as_tibble() %>% 
  select(Species, everything())

# Como funciona o group_by

# O mutate apos o group_by recebe cada uma das colunas com os registros do grupo
iris %>% 
  group_by(Species) %>% 
  summarise(
    Media = mean(Sepal.Length)
  ) %>% 
  ungroup()

# Na verdade o group by e quebrar em list e o lapply faz o papel do summarise
## Nesse exemplo NAO ha vantagem
iris %>% 
  split(.$Species) %>% 
  lapply(
    FUN = function(tabela){
      tabela %>% 
        group_by(Species) %>% 
        summarise(
          Media = mean(Sepal.Length)
        )
    }
  ) %>% 
  bind_rows()

# O mesmo lapply com paralelismo
iris %>% 
  split(.$Species) %>% 
  mclapply(
    FUN = function(tabela){
      tabela %>% 
        group_by(Species) %>% 
        summarise(
          Media = mean(Sepal.Length)
        )
    },
    mc.preschedule = TRUE,
    mc.cores = (parallel::detectCores() - 1) # Isso so funciona em Linux
  ) %>% 
  bind_rows()

####
## Fim
#